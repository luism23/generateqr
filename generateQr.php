<?php
/*
Plugin Name: Generate qr
Plugin URI: https://gitlab.com/luism23/generateqr
Description: Template Generate Pdf
Author: LuisMiguelNarvaez
Version: 1.0.1
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/generateqr',
	__FILE__,
	'Generate Qr'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');

define("GQR_PATH",plugin_dir_path(__FILE__));
define("GQR_URL",plugin_dir_url(__FILE__));

function GQR_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}



require TGP_PATH."shortcode/showFormByProduct.php";